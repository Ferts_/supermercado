import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import{ConfigModule} from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';
import { DB_HOST, DB_PORT, DB_PASSWORD, DB_USER, DB_DATABASE } from './config/constants';
import { EncargadosModule } from './encargados/encargados.module';
import { SupermercadoModule } from './supermercado/supermercado.module';
import { DepartamentosModule } from './departamentos/departamentos.module';
import { TrabajadoresModule } from './trabajadores/trabajadores.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule], 
      useFactory: (configService: ConfigService) => ({
        type: 'mariadb',
        host: configService.get<string>(DB_HOST),
        port: +configService.get<number>(DB_PORT),
        username: configService.get<string>(DB_USER),
        password: configService.get<string>(DB_PASSWORD),
        database: configService.get<string>(DB_DATABASE),
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
    
         synchronize: true,
        logging: false
      
      }),
      inject: [ConfigService],
    }),
    SupermercadoModule,EncargadosModule, DepartamentosModule, TrabajadoresModule
  ],
  controllers: [AppController,],
  providers: [AppService],
})
export class AppModule {}
