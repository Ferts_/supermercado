import { EntityRepository, Repository } from "typeorm";
import { Supermercado } from './supermercado.entity';

@EntityRepository(Supermercado)
export class supermercadoRepository extends Repository<Supermercado> {}