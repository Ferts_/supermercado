import{Module} from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SupermercadoController } from './supermercado.controller';
import { Supermercado } from './supermercado.entity';
import { SupermercadoService } from './supermercado.service';

@Module({
    imports: [TypeOrmModule.forFeature([Supermercado])],
    providers: [SupermercadoService],
    controllers: [SupermercadoController]
})
export class SupermercadoModule{

}