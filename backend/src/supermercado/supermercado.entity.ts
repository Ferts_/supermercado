import { IsPostalCode, IsNotEmpty } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany } from 'typeorm';
import { Departamentos } from '../departamentos/departamentos.entity';
import { Encargados } from '../encargados/encargados.entity';
@Entity()
export class Supermercado {
  @PrimaryGeneratedColumn()
  id: number;

  @IsNotEmpty()
  @Column({nullable: false})
  nombre: string;

  @IsNotEmpty()
  @Column({nullable: false})
  calle: string;

  @IsNotEmpty()
  @Column({nullable: false})
  numero: string;

  @IsNotEmpty()
  @Column({nullable: false})
  codigoPostal: string;
@IsNotEmpty()  
  @Column({nullable: false})
  colonia: string;
@IsNotEmpty()
  @Column({nullable: false})
  estado: string;
@IsNotEmpty()
  @Column({nullable: false})
  ciudad: string;
@IsNotEmpty()
  @Column({nullable: false})
  razonsocial: string;
@IsNotEmpty()
  @Column({nullable: false})
  correo: string;
@IsNotEmpty()
  @Column({type: 'varchar'})
  telefono: string;
  
  @OneToOne(() => Encargados, encargados => encargados.supermercado, {eager:true}) // specify inverse side as a second parameter
  encargados: Encargados[];

  @OneToMany(() => Departamentos, departamentos => departamentos.supermercado)
  departamentos: Departamentos[];
}