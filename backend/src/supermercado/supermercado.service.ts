import { InjectRepository } from '@nestjs/typeorm';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';


import { MessageDto } from 'src/common/message.dto';
import { Supermercado } from './supermercado.entity';
import { supermercadoRepository } from './supermercado.repository';
import { SupermercadoDto } from 'src/dto/supermercado.dto';


@Injectable()
export class SupermercadoService {

    constructor( 
        @InjectRepository(Supermercado)
        private supermercadoRepository: supermercadoRepository
   
         ){}  
         async getAll(): Promise<Supermercado[]> {
            const lista = await this.supermercadoRepository.find({relations: ['encargados']});
            if (!lista.length) {
                throw new NotFoundException(new MessageDto('la lista está vacía'));
            }
            return lista;
        }
        
  
        async findById(id: number): Promise<Supermercado> {
            const supermercado = await this.supermercadoRepository.findOne(id, {relations: ['encargados']});
            if (!supermercado) {
                throw new NotFoundException(new MessageDto('no existe'));
            }
            return supermercado;
        }
    
        async findByNombre(nombre: string): Promise<Supermercado> {
            const supermercado = await this.supermercadoRepository.findOne({ nombre: nombre });
            return supermercado;
        }
        async findByCorreo(correo:string): Promise<Supermercado> {
            const ccorreo = await this.supermercadoRepository.findOne({correo:correo});
            return ccorreo;
        }
    

        async findByRazon(razonsocial:string): Promise<Supermercado> {
            const rsocial = await this.supermercadoRepository.findOne({razonsocial:razonsocial});
            return rsocial;
        }
        async create(dto: SupermercadoDto): Promise<any> {
            const cesists = await this.findByCorreo(dto.correo);
            if (cesists) throw new BadRequestException(new MessageDto('Ese correo ya existe'));

            const supermercado = this.supermercadoRepository.create(dto);
            const esists = await this.findByRazon(dto.razonsocial);
            if (esists) throw new BadRequestException(new MessageDto('Esa razonsocial ya existe'));
            await this.supermercadoRepository.save(supermercado);
            return new MessageDto(`encargado ${supermercado.nombre} creado`);

            
        } 

}
