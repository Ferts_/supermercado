import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import { SupermercadoService } from './supermercado.service';
import { SupermercadoDto } from 'src/dto/supermercado.dto';

@Controller('/supermercado')
export class SupermercadoController {

    constructor( private readonly supermercadoService: SupermercadoService){}
    @Get()
    async getAll() {
        return await this.supermercadoService.getAll();
    }

    @Get(':id')
    async getOne(@Param('id', ParseIntPipe) id: number) {
        return await this.supermercadoService.findById(id);
    }

    @UsePipes(new ValidationPipe({whitelist: true}))
    @Post()
    async create(@Body() dto: SupermercadoDto) {
        return await this.supermercadoService.create(dto);
    }
    
}