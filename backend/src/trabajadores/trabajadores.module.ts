import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { Trabajadores } from "./trabajadores.entity";
import { TrabajadoresService } from "./trabajadores.service";
import { TrabajadoresController } from './trabajadores.controller';


@Module({
    imports: [TypeOrmModule.forFeature([Trabajadores])],
    providers: [TrabajadoresService],
    controllers: [TrabajadoresController]
})
export class TrabajadoresModule {}