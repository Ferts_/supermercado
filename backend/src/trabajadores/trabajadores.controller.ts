import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import { TrabajadoresDto } from 'src/dto/trabajadores.dto';

import { TrabajadoresService } from './trabajadores.service';

@Controller('/trabajadores')
export class TrabajadoresController {

    constructor( private readonly encargadosService: TrabajadoresService){}
    @Get()
    async getAll() {
        return await this.encargadosService.getAll();
    }

    @Get(':id')
    async getOne(@Param('id', ParseIntPipe) id: string) {
        return await this.encargadosService.findById(id);
    }

    @UsePipes(new ValidationPipe({whitelist: true}))
    @Post()
    async create(@Body() dto: TrabajadoresDto) {
        return await this.encargadosService.create(dto);
    }
    
    @UsePipes(new ValidationPipe({whitelist: true}))
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: string, @Body() dto:TrabajadoresDto) {
        return await this.encargadosService.update(id, dto);
    }

    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: string){
        return await this.encargadosService.delete(id)
    }
}