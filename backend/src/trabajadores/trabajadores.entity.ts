import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn,ManyToOne } from 'typeorm';
//Se importa la entidad de la que se sacara la llave foranea
import { Departamentos } from 'src/departamentos/departamentos.entity';
import { IsNotEmpty } from 'class-validator';
@Entity()
export class Trabajadores {
  @PrimaryGeneratedColumn()
  id: number;


  @Column({nullable: false})
  identificador: string;

  @IsNotEmpty()
  @Column({nullable: false})
  nombre: string;

  @IsNotEmpty()
  @Column({nullable: false})
  apellidoP: string;

  @IsNotEmpty()
  @Column({nullable: false})
  apellidoM: string;

  @IsNotEmpty()
  @Column({nullable: false})
  diasLaborales: string;

  @IsNotEmpty()
  @Column({type: 'varchar', nullable: false})
  telefono: string;

  @IsNotEmpty()
  @Column({nullable: false})
  departamento: string;
  
  @ManyToOne(() => Departamentos, departamentos => departamentos.trabajadores, { eager:true})
    departamentos: Trabajadores[];

 
  
}