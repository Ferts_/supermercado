import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { MessageDto } from 'src/common/message.dto';
import { TrabajadoresDto } from 'src/dto/trabajadores.dto';
import { Trabajadores } from "./trabajadores.entity";
import { TrabajadoresRepository } from './trabajadores.repository';

@Injectable()
export class TrabajadoresService {

    constructor( 
        @InjectRepository(Trabajadores)
        private trabajadoresRepository: TrabajadoresRepository  
         ){}  
         
         async getAll(): Promise<Trabajadores[]> {
            const list = await this.trabajadoresRepository.find({relations: ['departamentos']});
            if (!list.length) {
                throw new NotFoundException(new MessageDto('la lista está vacía'));
            }
            return list;
        }
    
        async findById(id: string): Promise<Trabajadores> {
            const trabajadores = await this.trabajadoresRepository.findOne(id, {relations: ['departamentos']});
            if (!trabajadores) {
                throw new NotFoundException(new MessageDto('no existe'));
            }
            return trabajadores;
        }
    
        async findByNombre(nombre: string): Promise<Trabajadores> {
            const trabajadores = await this.trabajadoresRepository.findOne({ nombre: nombre });
            return trabajadores;
        }


        async create(dto: TrabajadoresDto): Promise<any> {
            const trabajadores = this.trabajadoresRepository.create(dto);
            await this.trabajadoresRepository.save(trabajadores);
            return new MessageDto(`encargado ${trabajadores.nombre} creado`);

            
        }
    
        async update(identificador: string, dto: TrabajadoresDto): Promise<any> {
            const trabajadores = await this.findById(identificador);
            if (!trabajadores)
                throw new NotFoundException(new MessageDto('no existe'));
            const exists = await this.findByNombre(dto.nombre);
            if (exists && exists.identificador !== identificador) throw new BadRequestException(new MessageDto('ese trabajadores ya existe'));
            dto.nombre ? trabajadores.nombre = dto.nombre : trabajadores.nombre = trabajadores.nombre;
            dto.apellidoP ? trabajadores.apellidoP = dto.apellidoP : trabajadores.apellidoP = trabajadores.apellidoP;
            await this.trabajadoresRepository.save(trabajadores);
            return new MessageDto(`encargado ${trabajadores.nombre} actualizado`);
        }
    
        async delete(id: string): Promise<any> {
            const trabajadores = await this.findById(id);
            await this.trabajadoresRepository.delete(trabajadores);
            return new MessageDto(`encargado ${trabajadores.nombre} eliminado`);
        } 

}
