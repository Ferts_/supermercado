import { Trabajadores } from './trabajadores.entity';
import { Repository, EntityRepository } from 'typeorm';

@EntityRepository(Trabajadores)
export class TrabajadoresRepository extends Repository<Trabajadores> {}