import { Module } from "@nestjs/common";
import { DepartamentosService } from "./departamentos.service";
import { DepartamentosController } from './departamentos.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Departamentos } from './departamentos.entity';


@Module({
    imports: [TypeOrmModule.forFeature([Departamentos])],
    providers: [DepartamentosService],
    controllers: [DepartamentosController]
})
export class DepartamentosModule {}