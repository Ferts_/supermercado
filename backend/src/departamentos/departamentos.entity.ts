import { Entity, Column, PrimaryGeneratedColumn, OneToMany, OneToOne, JoinColumn , ManyToOne} from 'typeorm';
import { Trabajadores } from 'src/trabajadores/trabajadores.entity';
import { Supermercado } from 'src/supermercado/supermercado.entity';
import { IsNotEmpty } from 'class-validator';
@Entity()
export class Departamentos {
  @PrimaryGeneratedColumn()
  id: number;
  
  @IsNotEmpty()
  @Column({nullable: false})
  nombre: string;

  @OneToMany(() => Trabajadores, trabajadores => trabajadores.departamentos)
  trabajadores: Trabajadores[];

  @ManyToOne(() => Supermercado, supermercado => supermercado.departamentos, {eager:true, cascade:true})
   @JoinColumn({name: 'supermercadoId'})
  supermercado: Supermercado[];

}