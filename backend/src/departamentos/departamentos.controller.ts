import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import { DepartamentosService } from './departamentos.service';
import { EncargadosDto } from '../dto/encargados.dto';
import { DepartamentosDto } from 'src/dto/departamentos.dto';

@Controller('/departamentos')
export class DepartamentosController {

    constructor( private readonly departamentosService: DepartamentosService){}
    @Get()
    async getAll() {
        return await this.departamentosService.getAll();
    }

    @Get(':id')
    async getOne(@Param('id', ParseIntPipe) id: number) {
        return await this.departamentosService.findById(id);
    }

    @UsePipes(new ValidationPipe({whitelist: true}))
    @Post()
    async create(@Body() dto: DepartamentosDto) {
        return await this.departamentosService.create(dto);
    }

    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number){
        return await this.departamentosService.delete(id)
    }
}