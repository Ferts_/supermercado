import { EntityRepository, Repository } from "typeorm";
import {Departamentos} from './departamentos.entity';

@EntityRepository(Departamentos)
export class DepartamentosRepository extends Repository<Departamentos> {}