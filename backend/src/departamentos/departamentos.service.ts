import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { MessageDto } from "src/common/message.dto";
import { DepartamentosDto } from "src/dto/departamentos.dto";
import { Departamentos } from "./departamentos.entity";
import { DepartamentosRepository } from "./departamentos.repository";


@Injectable()
export class DepartamentosService {

    constructor( 
        @InjectRepository(Departamentos)
        private departamentosRepository: DepartamentosRepository
   
         ){}  
         
         async getAll(): Promise<Departamentos[]> {
            const list = await this.departamentosRepository.find({relations: ['supermercado']});
            if (!list.length) {
                throw new NotFoundException(new MessageDto('la lista está vacía'));
            }
            return list;
        }
    
        async findById(id: number): Promise<Departamentos> {
            const departamentos = await this.departamentosRepository.findOne(id, {relations: ['supermercado']});
            if (!departamentos) {
                throw new NotFoundException(new MessageDto('no existe'));
            }
            return departamentos;
        }
    
        async findByNombre(nombre: string): Promise<Departamentos> {
            const departamentos = await this.departamentosRepository.findOne({ nombre: nombre });
            return departamentos;
        }


        async create(dto: DepartamentosDto): Promise<any> {
            
            const cesists = await this.findByNombre(dto.nombre);
            if (cesists) throw new BadRequestException(new MessageDto('Ese departamento ya existe'));
            const departamentos = this.departamentosRepository.create(dto);
            await this.departamentosRepository.save(departamentos);
            return new MessageDto(`departamento ${departamentos.nombre} creado`);

            
        }
    

        async delete(id: number): Promise<any> {
            const departamentos = await this.findById(id);
            await this.departamentosRepository.delete(departamentos);
            return new MessageDto(`departamento ${departamentos.nombre} eliminado`);
        } 

}
