import { IsNotEmpty, IsNumber, IsNumberString, IsString, MaxLength, Min } from "class-validator";
import { IsNotBlank } from "../decorators/is-not-bank.decorator";


export class TrabajadoresDto {
    @IsNotBlank({message: 'la no puede estar vacía'})
    identificador: string;

    @IsNotBlank({message: 'el nombre no puede estar vacío'})
    nombre: string;
    
    apellidoP: string;
    
    apellidoM: string;
    
    diasLaborales: string;
    
    @IsNotBlank({message: 'el telefono no puede estar vacío'})
    telefono: string;

    departamento: string;
  
}