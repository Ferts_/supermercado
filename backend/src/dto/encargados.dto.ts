import { IsEmail, IsNotEmpty, IsNumber, IsNumberString, IsString, MaxLength, Min, MinLength } from "class-validator";
import { IsNotBlank } from "../decorators/is-not-bank.decorator";


export class EncargadosDto {
    @IsNotBlank({message: 'el nombre no puede estar vacío'})
    nombre: string;
    
    @IsNotBlank({message: 'el apellido paterno no puede estar vacío'})
    apellidoP: string;
    
    @IsNotBlank({message: 'el apellido materno no puede estar vacío'})
    apellidoM: string;
    
    @IsEmail()
    @IsNotBlank({message: 'el correo no puede estar vacío'})
    correo: string;
    
    @IsNotBlank({message: 'la fecha de nacimiento no puede estar vacío'})
    fechaN: string;
    
    @IsNotBlank({message: 'el genero no puede estar vacío'})
    Genero: string;
    
    @IsNotBlank({message: 'el telefono no puede estar vacío'})
    telefono: string;
    
    @MinLength(8, {message: 'la contraseña debe ser de minimo 8 caracteres'})
    @IsNotBlank({message: 'la contraseña no puede estar vacía'})
    password: string;
    
    @IsNotBlank({message: 'la contraseña no puede estar vacia'})
    rpassword: string;
  
}