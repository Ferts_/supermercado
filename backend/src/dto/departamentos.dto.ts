
import { IsNotBlank } from "../decorators/is-not-bank.decorator";
export class DepartamentosDto {
    @IsNotBlank({message: 'el nombre no puede estar vacío'})
    nombre: string;
 
  
}