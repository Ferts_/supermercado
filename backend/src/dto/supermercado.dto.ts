import { IsEmail, IsNotEmpty, IsNumber, IsNumberString, IsPostalCode, IsString, MaxLength, Min } from "class-validator";
import { IsNotBlank } from "../decorators/is-not-bank.decorator";


export class SupermercadoDto {
    @IsNotBlank({message: 'el nombre no puede estar vacío'})
    nombre: string;
    
    @IsNotBlank({message: 'la calle no puede estar vacía'})
    calle: string;
   
    @IsNotBlank({message: 'el numero no puede estar vacío'})
    numero: string;

    @IsNotBlank({message: 'la colonia no puede estar vacia'})
    colonia: string;

    @IsNotBlank({message: 'el estado no puede estar vacío'})
    estado: string;
   
    @IsNotBlank({message: 'la ciudad no puede estar vacia'})
    ciudad: string;
   
    @IsNotBlank({message: 'la razon social no puede estar vacía'})
    razonsocial: string;
   
    @IsNotBlank({message: 'el correo no puede estar vacío'})
    correo: string;

    
    @IsNotBlank({message: 'el codigo postal no puede estar vacío'})
    codigoPostal: string;
   
    @IsNotBlank({message: 'el telefono no puede estar vacío'})
    telefono: string;

    

  
}