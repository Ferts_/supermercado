import { Module } from "@nestjs/common";
import { EncargadosService } from "./encargados.service";
import { EncargadosController } from './encargados.controller';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Encargados } from './encargados.entity';


@Module({
    imports: [TypeOrmModule.forFeature([Encargados])],
    providers: [EncargadosService],
    controllers: [EncargadosController]
})
export class EncargadosModule {}