import { Encargados } from './encargados.entity';
import { EntityRepository, Repository } from "typeorm";

@EntityRepository(Encargados)
export class EncargadosRepository extends Repository<Encargados> {}