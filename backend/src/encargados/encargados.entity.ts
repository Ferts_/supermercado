import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn } from 'typeorm';
//Se importa la entidad de la que se sacara la llave foranea
import { Supermercado } from 'src/supermercado/supermercado.entity';
import { IsNotEmpty , IsEmail, IsNumberString} from 'class-validator';

@Entity()
export class Encargados {
  @PrimaryGeneratedColumn()
  id: number;

  @IsNotEmpty()
  @Column({nullable: false})
  nombre: string;

  @IsNotEmpty()
  @Column({nullable: false})
  apellidoP: string;

  @IsNotEmpty()
  @Column({nullable: false})
  apellidoM: string;

  @IsNotEmpty()
  @IsEmail()
  @Column({nullable: false})
  correo: string;

  @IsNotEmpty()
  @Column({type: 'varchar', nullable: false})
  fechaN: string;

  @IsNotEmpty()
  @Column({nullable: false})
  Genero: string;

  
  @Column({type: 'varchar', nullable: false})
  @IsNumberString()
  telefono: string;

  @IsNotEmpty()
  @Column({nullable: false})
  password: string;

  @IsNotEmpty()
  @Column({nullable: false})
  rpassword: string;

  
   @OneToOne(() => Supermercado, supermercado => supermercado.encargados, {cascade: true}) // specify inverse side as a second parameter
  @JoinColumn()
   supermercado: Encargados[];
  
}