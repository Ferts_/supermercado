import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, UseGuards, UsePipes, ValidationPipe} from '@nestjs/common';
import { EncargadosService } from './encargados.service';
import { EncargadosDto } from '../dto/encargados.dto';

@Controller('/encargados')
export class EncargadosController {

    constructor( private readonly encargadosService: EncargadosService){}
    @Get()
    async getAll() {
        return await this.encargadosService.getAll();
    }

    @Get(':id')
    async getOne(@Param('id', ParseIntPipe) id: number) {
        return await this.encargadosService.findById(id);
    }

    @UsePipes(new ValidationPipe({whitelist: true}))
    @Post()
    async create(@Body() dto: EncargadosDto) {
        return await this.encargadosService.create(dto);
    }

    @UsePipes(new ValidationPipe({whitelist: true}))
    @Put(':id')
    async update(@Param('id', ParseIntPipe) id: number, @Body() dto: EncargadosDto) {
        return await this.encargadosService.update(id, dto);
    }

    @Delete(':id')
    async delete(@Param('id', ParseIntPipe) id: number){
        return await this.encargadosService.delete(id)
    }
}