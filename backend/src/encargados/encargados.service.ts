import { InjectRepository } from '@nestjs/typeorm';
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';

import { Encargados } from './encargados.entity';
import { EncargadosRepository } from './encargados.repository';
import { EncargadosDto } from '../dto/encargados.dto';
import { MessageDto } from 'src/common/message.dto';
import { Supermercado } from 'src/supermercado/supermercado.entity';



@Injectable()
export class EncargadosService {

    constructor( 
        @InjectRepository(Encargados)
        private encargadosRepository: EncargadosRepository
   
         ){}  
         
         async getAll(): Promise<Encargados[]> {
            const lista = await this.encargadosRepository.find({relations: ['supermercado']});
            if (!lista.length) {
                throw new NotFoundException(new MessageDto('la lista está vacía'));
            }
            return lista;
        }
    
        async findById(id: number): Promise<Encargados> {
            const encargados = await this.encargadosRepository.findOne(id, {relations: ['supermercado']});
            if (!encargados) {
                throw new NotFoundException(new MessageDto('no existe'));
            }
            return encargados;
        }
    
        async findByNombre(nombre: string): Promise<Encargados> {
            const encargados = await this.encargadosRepository.findOne({ nombre: nombre });
            return encargados;
        }
        async findByCorreo(correo:string): Promise<Encargados> {
            const ccorreo = await this.encargadosRepository.findOne({correo:correo});
            return ccorreo;
        }
        async comparar(password: string, rpassword: string): Promise<Encargados[]> {
            const niguales = await this.encargadosRepository.find({password:rpassword});
            if (password != rpassword) {
                throw new NotFoundException(new MessageDto('Las contraseñas no coinciden'))
            }
            return  niguales;
        }

        async create(dto: EncargadosDto): Promise<any> {
            const ciguales = await this.comparar(dto.password, dto.rpassword);
            if (!ciguales) throw new BadRequestException(new MessageDto('Las contraseñas no coinciden'));
            const cesists = await this.findByCorreo(dto.correo);
            if (cesists) throw new BadRequestException(new MessageDto('Ese correo ya existe'));
            const encargados = this.encargadosRepository.create(dto);
            await this.encargadosRepository.save(encargados);
            return new MessageDto(`encargado ${encargados.nombre} creado`);

            
        }
    
        async update(id: number, dto: EncargadosDto): Promise<any> {
            const encargados = await this.findById(id);
            if (!encargados)
                throw new NotFoundException(new MessageDto('no existe'));
            const exists = await this.findByNombre(dto.nombre);
            if (exists && exists.id !== id) throw new BadRequestException(new MessageDto('ese encargados ya existe'));
            dto.nombre ? encargados.nombre = dto.nombre : encargados.nombre = encargados.nombre;
            dto.apellidoP ? encargados.apellidoP = dto.apellidoP : encargados.apellidoP = encargados.apellidoP;
            
            dto.apellidoM ? encargados.apellidoM = dto.apellidoM : encargados.apellidoM = encargados.apellidoM;
            
            dto.correo ? encargados.correo = dto.correo : encargados.correo = encargados.correo;
            
            dto.Genero ? encargados.Genero = dto.Genero : encargados.Genero = encargados.Genero;
            
            dto.telefono ? encargados.telefono = dto.telefono : encargados.telefono = encargados.telefono;
            
            dto.fechaN ? encargados.fechaN = dto.fechaN : encargados.fechaN = encargados.fechaN;
            
            dto.password ? encargados.password = dto.password : encargados.password = encargados.password;
            
            dto.rpassword ? encargados.rpassword = dto.rpassword : encargados.rpassword = encargados.rpassword;
            await this.encargadosRepository.save(encargados);
            return new MessageDto(`encargado ${encargados.nombre} actualizado`);
        }
    
        async delete(id: number): Promise<any> {
            const encargados = await this.findById(id);
            await this.encargadosRepository.delete(encargados);
            return new MessageDto(`encargado ${encargados.nombre} eliminado`);
        } 

}
