import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// external
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { ListaEncargadosComponent } from './encargados/lista-encargados.component';
import { EditarEncargadosComponent } from './encargados/editar-encargados.component';
import { NgxMaskModule } from 'ngx-mask';
import { NuevoEncargadosComponent } from './encargados/nuevo-encargados.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { NuevoSupermercadoComponent } from './supermercado/nuevo-supermercado.component';
import { ListaSupermercadoComponent } from './supermercado/lista-supermercado.component';
import { DetalleSupermercadoComponent } from './supermercado/detalle-supermercado.component';
import { HomeComponent } from './home/home.component';
import { VacioPipe } from './pipes/vacio.pipe';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarEComponent } from './navbar-e/navbar-e.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { ListaDepartamentosComponent } from './departamentos/lista-departamentos.component';
import { DepartamentosEliminarComponent } from './departamentos/departamentos-eliminar.component';
import { NuevoTrabajadoresComponent } from './trabajadores/nuevo-trabajadores.component';
import { ListaTrabajadoresComponent } from './trabajadores/lista-trabajadores.component';

@NgModule({
  declarations: [
    AppComponent,
    ListaEncargadosComponent,
    EditarEncargadosComponent,
    NuevoEncargadosComponent,
    NuevoSupermercadoComponent,
    ListaSupermercadoComponent,
    DetalleSupermercadoComponent,
    HomeComponent,
    VacioPipe,
    NavbarComponent,
    LoginComponent,
    SidebarComponent,
    NavbarEComponent,
    DepartamentosComponent,
    ListaDepartamentosComponent,
    DepartamentosEliminarComponent,
    NuevoTrabajadoresComponent,
    ListaTrabajadoresComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    NgxMaskModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
