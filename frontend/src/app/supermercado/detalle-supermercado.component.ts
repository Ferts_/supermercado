import { Component, OnInit } from '@angular/core';
import { SupermercadoService } from '../services/supermercado.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Supermercado } from '../models/supermercado';

@Component({
  selector: 'app-detalle-supermercado',
  templateUrl: './detalle-supermercado.component.html',
  styleUrls: ['./detalle-supermercado.component.css']
})
export class DetalleSupermercadoComponent implements OnInit {

 
  supermercado : any = [];
  constructor(
    private supermercadoService: SupermercadoService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    this.supermercadoService.detail(id).subscribe(
      data => {

        this.supermercado = data;
        console.log(data);
        this.router.navigate([`/Supermercadodetalles/${id}`]);
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
      }
    );
  }
  
  cargarSupermercado(): void {
    const id = this.activatedRoute.snapshot.params['id'];
  this.supermercadoService.detail(id).subscribe(
    data => {
      console.log(data);
    },
    err => {
      this.toastr.error(err.error.message, 'Error', {
        timeOut: 5000,  positionClass: 'toast-top-center',
        
      });
      console.log(err);
    }
  );
  }


  volver(): void {
    this.router.navigate(['/']);
  }


}
