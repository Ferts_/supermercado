import { Component, OnInit } from '@angular/core';
import { Supermercado } from '../models/supermercado';
import { SupermercadoService } from '../services/supermercado.service';

@Component({
  selector: 'app-lista-supermercado',
  templateUrl: './lista-supermercado.component.html',
  styleUrls: ['./lista-supermercado.component.css']
})
export class ListaSupermercadoComponent implements OnInit {

  supermercado: any=[];
  public page : number | undefined;
  constructor(
    private supermercadoService: SupermercadoService,
    ) { }

  ngOnInit(): void{
    this.cargarSupermercado();
  }

  cargarSupermercado(): void {
    this.supermercadoService.lista()
    .subscribe(
      data => {
        this.supermercado = data;
        console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }


}
