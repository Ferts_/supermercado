import { Component, OnInit } from '@angular/core';
import { Supermercado } from '../models/supermercado';
import { SupermercadoService } from '../services/supermercado.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import SweetAlert2 from 'sweetalert2';
import { Encargados } from '../models/encargados';

@Component({
  selector: 'app-nuevo-supermercado',
  templateUrl: './nuevo-supermercado.component.html',
  styleUrls: ['./nuevo-supermercado.component.css']
})
export class NuevoSupermercadoComponent implements OnInit {

  supermercado: Supermercado = {
    nombre: '',
    calle: '',
    numero: '',
    codigoPostal: '',
    colonia: '',
    estado: '',
    ciudad: '',
    razonsocial: '',
    correo: '',
    telefono: '',
   
};
edit: boolean = false;
constructor(private supermercadoService: SupermercadoService,   private toastr: ToastrService,
  private router: Router) { }

ngOnInit() : void{
   
}
crearSupermercado(): void{
  this.supermercadoService.createSupermercado(this.supermercado)
  .subscribe( data => {
    this.showModal();
   },
   err => {
     
     this.toastr.error(err.error.message, 'Error', {
       timeOut: 3000,  positionClass: 'toast-top-center',
     });
   }
  );
}


showModal(){
SweetAlert2.fire({
  title: '¡Registro Exitoso!',
  text: "Supermercado Guardado correctamente",
  icon: 'success',
  confirmButtonColor: '#017436',
  confirmButtonText: 'ACEPTAR',
}).then((result) => {
  if (result.value) {
     
      this.router.navigate(['Supermercadolista']);
  }
});
}


}
