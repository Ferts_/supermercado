import { Component, OnInit } from '@angular/core';
import { DepartamentosService } from '../services/departamentos.service';
import { Departamentos } from '../models/departamentos.models';
import SweetAlert2 from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-lista-departamentos',
  templateUrl: './lista-departamentos.component.html',
  styleUrls: ['./lista-departamentos.component.css']
})
export class ListaDepartamentosComponent implements OnInit {

  departamentos: any = [];
  public page : number | undefined;
  vacio : any = ' NULL ';
  
  departamento: Departamentos = {
    nombre: ''}
  constructor(
    private departamentosService: DepartamentosService,
    private router: Router, private toastr: ToastrService, private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void{
    this.cargarDepartamentos();
  }
  cargarDepartamentos(): void {
    
    this.departamentosService.lista().subscribe(

      complete => {
        this.departamentos = complete;
        console.log(this.departamentos);
      },
      error => {
        console.log(error);
      }
    );
  }

  depa(){
    this.showModal();

  }
  async showModal(){
    const { value: formValues} = await SweetAlert2.fire({
      title: 'Agregar Departamento',
      confirmButtonText: 'Guardar',
      html:
      '<input id="nombre" [(ngModel)]="departamentos.nombre" class="swal2-input">',
      
    })
    if (formValues == true) {
     this.crearDepartamento();
    }
  }

  showModal2(){
    SweetAlert2.fire({
      title: '¡Registro Exitoso!',
      text: "Departamento Guardado correctamente",
      icon: 'success',
      confirmButtonColor: '#017436',
      confirmButtonText: 'ACEPTAR',
  }).then((result) => {
      if (result.value) {
         
          this.router.navigate(['Departamentoslista']);
      }
  });
  }
  
  crearDepartamento(): void{
    this.departamentosService.createDepartamentos(this.departamentos)
    .subscribe( data => {
      this.showModal2();
     },
     err => {
       this.toastr.error(err.error.message, 'Error', {
         timeOut: 3000,  positionClass: 'toast-top-center',
       });
     }
    );
  }

  borrar(id: number): void {
    
  alert(id);
    this.departamentosService.delete(id).subscribe(
      data => {
        this.showModal();
      },
      err => {
        this.toastr.error(err.error.message, 'Error', {
          timeOut: 5000,  positionClass: 'toast-top-center',
          
        });
        console.log(err);
      }
    );
  }
  
}
