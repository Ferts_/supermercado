import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartamentosEliminarComponent } from './departamentos-eliminar.component';

describe('DepartamentosEliminarComponent', () => {
  let component: DepartamentosEliminarComponent;
  let fixture: ComponentFixture<DepartamentosEliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartamentosEliminarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartamentosEliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
