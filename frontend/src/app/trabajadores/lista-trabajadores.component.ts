import { Component, OnInit } from '@angular/core';
import { SupermercadoService } from '../services/supermercado.service';
import { TrabajadoresService } from '../services/trabajadores.service';

@Component({
  selector: 'app-lista-trabajadores',
  templateUrl: './lista-trabajadores.component.html',
  styleUrls: ['./lista-trabajadores.component.css']
})
export class ListaTrabajadoresComponent implements OnInit {

  trabajadores: any = [];
  public page : number | undefined;
  vacio : any = ' NULL '
  constructor(
    private trabajadoresService: TrabajadoresService,
    ) { }

  ngOnInit(): void{
    this.cargarTrabajadores();
  }
  cargarTrabajadores(): void {
    
    this.trabajadoresService.lista().subscribe(

      complete => {
        this.trabajadores = complete;
        console.log(this.trabajadores);
      },
      error => {
        console.log(error);
      }
    );
  }

}
