import { Component, OnInit } from '@angular/core';
import { Trabajadores } from '../models/trabajadores.models';
import { TrabajadoresService } from '../services/trabajadores.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

import SweetAlert2 from 'sweetalert2'
@Component({
  selector: 'app-nuevo-trabajadores',
  templateUrl: './nuevo-trabajadores.component.html',
  styleUrls: ['./nuevo-trabajadores.component.css']
})
export class NuevoTrabajadoresComponent implements OnInit {


  trabajadores: Trabajadores = {
    nombre: '',
    apellidoP: '',
    apellidoM : '', 
    diasLaborales : '',
    departamento : '',
    identificador : '',
    telefono: '',
};
edit: boolean = false;
constructor(private trabajadoresService: TrabajadoresService,   private toastr: ToastrService,
  private router: Router, private activatedRoute: ActivatedRoute) { }

ngOnInit() : void{
   
}
crearEncargado(): void{
  this.trabajadoresService.createTrabajadores(this.trabajadores)
  .subscribe( data => {
    this.showModal();
   },
   err => {
     
     this.toastr.error(err.error.message, 'Error', {
       timeOut: 3000,  positionClass: 'toast-top-center',
     });
   }
  );
}


showModal(){
SweetAlert2.fire({
  title: '¡Registro Exitoso!',
  text: "Trabajador Guardado correctamente",
  icon: 'success',
  confirmButtonColor: '#017436',
  confirmButtonText: 'ACEPTAR',
}).then((result) => {
  if (result.value) {
     
      this.router.navigate(['/Trabajadoreslista']);
  }
});
}



}
