import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
   plataforma(){
document.getElementById("plataforma")?.scrollIntoView({behavior:"smooth"});
  }
  beneficios(){
    document.getElementById("beneficios")?.scrollIntoView({behavior:"smooth"});
  }
  nclientes(){
    document.getElementById("nclientes")?.scrollIntoView({behavior:"smooth"});
  }

  carrusel(){
    document.getElementById("carrusel")?.scrollIntoView({behavior:"smooth"});
  }
}
