import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Departamentos } from '../models/departamentos.models';

@Injectable({
  providedIn: 'root'
})
export class DepartamentosService {
  BASE_URL: string = 'http://localhost:7656/departamentos';
  departamentosURL = environment.departamentosURL;

  
  constructor(private http: HttpClient, private httpClient: HttpClient) { }

  public lista(): Observable<Departamentos[]> {
    return this.httpClient.get<Departamentos[]>(`${this.departamentosURL}`);
  }

  public detail(id: number): Observable<Departamentos> {
    return this.httpClient.get<Departamentos>(`${this.departamentosURL}${id}`);
  }

 

  createDepartamentos(departamentos: Departamentos): Observable<Departamentos> {
    return this.http.post<Departamentos>(`${this.BASE_URL}`, departamentos);
  }


  update(departamentos: Departamentos, id: number): Observable<Departamentos> {
    return this.http.put<Departamentos>(`${this.BASE_URL}/${id}`, departamentos);
  }

  
public delete(id: number): Observable<any> {  
return this.httpClient.delete<any>(`${this.departamentosURL}/${id}`)}
}
