import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Supermercado } from './../models/supermercado';

@Injectable({
  providedIn: 'root'
})
export class SupermercadoService {
  BASE_URL: string = 'http://localhost:7656/supermercado';
  supermercadoURL = environment.supermercadoURL;

  
  constructor(private http: HttpClient, private httpClient: HttpClient) { }

  public lista(): Observable<Supermercado[]> {
    return this.httpClient.get<Supermercado[]>(`${this.supermercadoURL}`);
  }


  public detail(id:number): Observable<Supermercado> {
    return this.httpClient.get<Supermercado>(`${this.supermercadoURL}${id}`);
  }

  createSupermercado(supermercado: Supermercado): Observable<Supermercado> {
    return this.http.post<Supermercado>(`${this.BASE_URL}`, supermercado);
  }
 
  update(supermercado: Supermercado, id: number): Observable<Supermercado> {
    return this.http.put<Supermercado>(`${this.BASE_URL}/${id}`, supermercado);
  }
}
