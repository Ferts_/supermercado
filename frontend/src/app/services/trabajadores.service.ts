import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trabajadores } from '../models/trabajadores.models';
@Injectable({
  providedIn: 'root'
})
export class TrabajadoresService {
  BASE_URL: string = 'http://localhost:7656/trabajadores';
  trabajadoresURL = environment.trabajadoresURL;

  
  constructor(private http: HttpClient, private httpClient: HttpClient) { }

  public lista(): Observable<Trabajadores[]> {
    return this.httpClient.get<Trabajadores[]>(`${this.trabajadoresURL}`);
  }


  public detail(id:number): Observable<Trabajadores> {
    return this.httpClient.get<Trabajadores>(`${this.trabajadoresURL}${id}`);
  }

  createTrabajadores(trabajadores: Trabajadores): Observable<Trabajadores> {
    return this.http.post<Trabajadores>(`${this.BASE_URL}`, trabajadores);
  }
 
  update(trabajadores: Trabajadores, id: number): Observable<Trabajadores> {
    return this.http.put<Trabajadores>(`${this.BASE_URL}/${id}`, trabajadores);
  }
}
