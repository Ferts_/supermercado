import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Encargados } from './../models/encargados';

@Injectable({
  providedIn: 'root'
})
export class EncargadosService {
  BASE_URL: string = 'http://localhost:7656/encargados';
  encargadosURL = environment.encargadosURL;

  
  constructor(private http: HttpClient, private httpClient: HttpClient) { }

  public lista(): Observable<Encargados[]> {
    return this.httpClient.get<Encargados[]>(`${this.encargadosURL}`);
  }

  public detail(id: number): Observable<Encargados> {
    return this.httpClient.get<Encargados>(`${this.encargadosURL}${id}`);
  }

 

  createEncargados(encargados: Encargados): Observable<Encargados> {
    return this.http.post<Encargados>(`${this.BASE_URL}`, encargados);
  }


  update(encargados: Encargados, id: number): Observable<Encargados> {
    return this.http.put<Encargados>(`${this.BASE_URL}/${id}`, encargados);
  }

  

  // public delete(id: number): Observable<any> {
  //   return this.httpClient.delete<any>(`${this.encargadosURL}${id}`);
  // }
}
