import { Component, OnInit } from '@angular/core';
import { EncargadosService } from '../services/encargados.service';

@Component({
  selector: 'app-lista-encargados',
  templateUrl: './lista-encargados.component.html',
  styleUrls: ['./lista-encargados.component.css']
})
export class ListaEncargadosComponent implements OnInit {

  encargados: any = [];
  public page : number | undefined;
  vacio : any = ' NULL '
  constructor(
    private encargadosService: EncargadosService,
    ) { }

  ngOnInit(): void{
    this.cargarEncargados();
  }
  cargarEncargados(): void {
    
    this.encargadosService.lista().subscribe(

      complete => {
        this.encargados = complete;
        console.log(this.encargados);
      },
      error => {
        console.log(error);
      }
    );
  }

}
