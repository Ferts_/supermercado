import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Encargados } from '../models/encargados';
import { EncargadosService } from '../services/encargados.service';
import SweetAlert2 from 'sweetalert2'

@Component({
  selector: 'app-nuevo-encargados',
  templateUrl: './nuevo-encargados.component.html',
  styleUrls: ['./nuevo-encargados.component.css']
})
export class NuevoEncargadosComponent implements OnInit {



  encargados: Encargados = {
      nombre: '',
      apellidoP: '',
      apellidoM : '', 
      correo: '',
      Genero: '',
      fechaN : '',
      telefono: '',
      password: '',
      rpassword: ''
  };
  edit: boolean = false;
  constructor(private encargadosService: EncargadosService,   private toastr: ToastrService,
    private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() : void{
     
  }
  crearEncargado(): void{
    this.encargadosService.createEncargados(this.encargados)
    .subscribe( data => {
      this.showModal();
     },
     err => {
       
       this.toastr.error(err.error.message, 'Error', {
         timeOut: 3000,  positionClass: 'toast-top-center',
       });
     }
    );
  }

  
 showModal(){
  SweetAlert2.fire({
    title: '¡Registro Exitoso!',
    text: "Encargado Guardado correctamente",
    icon: 'success',
    confirmButtonColor: '#017436',
    confirmButtonText: 'ACEPTAR',
}).then((result) => {
    if (result.value) {
       
        this.router.navigate(['Encargadoslista']);
    }
});
}



}
