import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Encargados } from '../models/encargados';
import { EncargadosService } from '../services/encargados.service';
import SweetAlert2 from 'sweetalert2'

@Component({
  selector: 'app-editar-encargados',
  templateUrl: './editar-encargados.component.html',
  styleUrls: ['./editar-encargados.component.css']
})
export class EditarEncargadosComponent implements OnInit {
  encargados: Encargados = {
    nombre: '',
    apellidoP: '',
    apellidoM : '', 
    correo: '',
    Genero: '',
    fechaN : '',
    telefono: '',
    password: '',
    rpassword: ''
};

constructor(
  private encargadosService: EncargadosService,
  private activatedRoute: ActivatedRoute,
  private toastr: ToastrService,
  private router: Router
) { }

ngOnInit(): void {
  const id = this.activatedRoute.snapshot.params['id'];
  this.encargadosService.detail(id).subscribe(
    data => {
      this.encargados = data;
    },
    err => {
      this.toastr.error(err.error.message, 'Fail', {
        timeOut: 5000,  positionClass: 'toast-top-center',
      });
     
    }
  );
}

onUpdate(): void {
  const id = this.activatedRoute.snapshot.params['id'];
  this.encargadosService.update(this.encargados, id).subscribe(
    data => {
      this.showModal();
    },
    err => {
      this.toastr.error(err.error.message, 'Error', {
        timeOut: 5000,  positionClass: 'toast-top-center',
        
      });
      console.log(err);
    }
  );
}

volver(): void {
  this.router.navigate(['/']);
}



 showModal(){
  SweetAlert2.fire({
    title: '¿Estás Seguro?',
    text: "Se modificaran los datos del Encargado",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#F08C00',
    cancelButtonColor: '#3C4650',
    confirmButtonText: 'ACEPTAR',
    cancelButtonText: 'CANCELAR'
}).then((result) => {
    if (result.isConfirmed) {
        SweetAlert2.fire({
          title: '¡Edición Exitosa!',
          text: "Los cambios se guardaron correctamente",
          icon: 'success',
          confirmButtonColor: '#017436',
          confirmButtonText: 'ACEPTAR',
        }    
        )
        this.router.navigate(['Encargadoslista']);
    }
});
}


}
