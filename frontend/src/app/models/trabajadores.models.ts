export class Trabajadores{
    id?: number;
    nombre: string;
    apellidoP: string;
    apellidoM: string;
    diasLaborales: string;       
    identificador: string;     
    departamento: string;       
    telefono: string;    
    constructor(nombre: string,apellidoP: string, apellidoM:string, diasLaborales:string, identificador:string, departamento: string,telefono:string){
    this.nombre = nombre;
    this.apellidoP= apellidoP;
    this.apellidoM= apellidoM;
    this.diasLaborales= diasLaborales;
    this.identificador = identificador;
    this.departamento = departamento;
    this.telefono  = telefono;
    }
}
