export class Supermercado {
id ?: number;
nombre: string;
calle: string;
numero: string;
codigoPostal: string;
colonia: string;
estado: string;
ciudad: string;
razonsocial: string;
correo: string;
telefono: string;
 
constructor(nombre: string,
    calle: string,
    numero: string,
    codigoPostal: string,
    colonia: string,
    estado: string,
    ciudad: string,
    razonsocial: string,
    correo: string,
    telefono: string,){
        this.nombre = nombre;
        this.calle = calle;
        this.numero = numero;
        this.codigoPostal = codigoPostal;
        this.colonia = colonia;
        this.estado = estado;
        this.ciudad = ciudad;
        this.razonsocial =razonsocial;
        this.correo = correo;
        this.telefono = telefono;
}

}
