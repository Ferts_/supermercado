export class Encargados {
       id?: number;
       nombre: string;
       apellidoP: string;
       apellidoM: string;
       correo: string;       
       fechaN: string;     
       Genero: string;       
       telefono: string;       
       password: string;      
       rpassword: string;
    
        constructor(
              nombre: string,    
              apellidoP: string,
              apellidoM: string,              
              correo: string,             
              fechaN: string,             
              Genero: string,              
              telefono: string,              
              password: string,
              rpassword: string) {
        this.nombre = nombre;
        this.apellidoP = apellidoP;
        this.apellidoM = apellidoM;
        this.correo = correo;
        this.fechaN = fechaN;
        this.Genero = Genero;
        this.telefono = telefono;
        this.password = password;
        this.rpassword = rpassword;
        }

    }

