import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { EditarEncargadosComponent } from './encargados/editar-encargados.component';
import { ListaEncargadosComponent } from './encargados/lista-encargados.component';
import { NuevoEncargadosComponent } from './encargados/nuevo-encargados.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DetalleSupermercadoComponent } from './supermercado/detalle-supermercado.component';
import { ListaSupermercadoComponent } from './supermercado/lista-supermercado.component';
import { NuevoSupermercadoComponent } from './supermercado/nuevo-supermercado.component';
import { ListaDepartamentosComponent } from './departamentos/lista-departamentos.component';
import { DepartamentosEliminarComponent } from './departamentos/departamentos-eliminar.component';
import { NuevoTrabajadoresComponent } from './trabajadores/nuevo-trabajadores.component';
import { ListaTrabajadoresComponent } from './trabajadores/lista-trabajadores.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'side', component: SidebarComponent},
  
  {path: 'login', component: LoginComponent},
  {path: 'Encargadoslista', component: ListaEncargadosComponent},
  
  {path: 'Encargadosnuevo', component: NuevoEncargadosComponent},
  {path: 'Encargadoseditar/:id', component: EditarEncargadosComponent},
  
  {path: 'Supermercadolista', component: ListaSupermercadoComponent},
  {path: 'Supermercadonuevo', component: NuevoSupermercadoComponent},
  {path: 'Supermercadodetalles/:id', component: DetalleSupermercadoComponent},
  {path: 'Departamentosnuevo', component: DepartamentosComponent},
  {path: 'Departamentoslista', component: ListaDepartamentosComponent},
  {path: 'Departamentoseliminar/:id', component: DepartamentosEliminarComponent},
  
  {path: 'Trabajadoresnuevo', component: NuevoTrabajadoresComponent},
  {path: 'Trabajadoreslista', component: ListaTrabajadoresComponent},

  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
